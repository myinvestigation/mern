var express = require('express');
var router = express.Router();

import * as UserController from '../controllers/user.controller';


router.get('/:id?',function(req,res,next){
    if(req.params.id){
        UserController.get(req, res);
    }else{
        UserController.index(req, res);
    }
});

router.post('/',function(req,res,next){
    UserController.create(req, res);
});

router.delete('/:id',function(req,res,next){
    UserController.delete(req, res);
});

router.put('/:id',function(req,res,next){
    UserController.update(req, res);
});

router.post('/login',function(req,res,next){
    UserController.login(req, res);
});

router.post('/logout',function(req,res,next){
    UserController.logout(req, res);
});

router.post('/info_login',function(req,res,next){
    // console.log("info_login")
    UserController.login_info(req, res);
});

export default router;
