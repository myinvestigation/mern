import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

// Import Style
import styles from './App.css';

// Import Components
import Helmet from 'react-helmet';
import DevTools from './components/DevTools';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import $ from 'jquery'


// Import Actions
import { toggleAddPost } from './AppActions';
import { switchLanguage } from '../../modules/Intl/IntlActions';

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = { 
        isMounted: false,
        is_login : false,
        user:{}
    };
    console.log("App: constructor")
    this.logout = this.logout.bind(this);
  }

  componentDidMount() {
    console.log("APP: componentDidMount");
    this.setState({isMounted: true}); // eslint-disable-line
    this.check_login();
  }
  check_login(){
      var this_tmp = this;
      $.ajax( {        
        url: "http://localhost:8000/api/users/info_login", 
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        success: function(rs){
          if (rs.success){
            console.log("check_login: success");
            this_tmp.setState({is_login: true, user:rs.user})
            
          }
          else{
            console.log("check_login: " + rs.message)
            // alert("LOI: "+ rs.message)
          }
          console.log(rs);
        },
        error: function(e){
          console.log("check_login: Loi")
          // alert("LOI: "+ rs.message)
        }
      });

   }

   logout(e){
      $.ajax( {        
        url: "http://localhost:8000/api/users/logout", 
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        success: function(rs){
          if (rs.success){
            // alert("Dang xuat thanh cong")
            location.reload();
          }
          else{
            alert("That bai: " + rs.message)
          }
          console.log(rs);
        },
        error: function(e){
          alert("Loi")
        }
      });
  }

  toggleAddPostSection = () => {
    this.props.dispatch(toggleAddPost());
  };

  render() {
    return (
      <div>
        {this.state.isMounted && !window.devToolsExtension && process.env.NODE_ENV === 'development' && <DevTools />}
        <div>
          <Helmet
            title="MERN Starter - Blog App"
            titleTemplate="%s - Blog App"
            meta={[
              { charset: 'utf-8' },
              {
                'http-equiv': 'X-UA-Compatible',
                content: 'IE=edge',
              },
              {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1',
              },
            ]}
          />
          <Header
            logout={this.logout}
            is_login={this.state.is_login}
            user={this.state.user}
            switchLanguage={lang => this.props.dispatch(switchLanguage(lang))}
            intl={this.props.intl}
            toggleAddPost={this.toggleAddPostSection}
          />
          <div className={styles.container}>
            {this.props.children}
          </div>
          <Footer />
        </div>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired,
};

// Retrieve data from store as props
function mapStateToProps(store) {
  return {
    intl: store.intl,
  };
}

export default connect(mapStateToProps)(App);
