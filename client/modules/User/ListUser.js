import React from 'react';
import $ from 'jquery'

class App extends React.Component {
   constructor(props) {
      super(props);
      
      this.state = {
         data: []
      }
      this.updateState = this.updateState.bind(this);

   };
   componentDidMount() {
      this.updateState()
   }

   updateState() {
      console.log("updateState")
      this.setState({data: []})
      var this_tmp = this;

      $.ajax( {        
        url: "http://localhost:8000/api/users", 
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        success: function(rs){
          this_tmp.setState({data:rs});
        }
      });

   };

   delete_user(e) {
      var id = e.target.attributes['data-id'].value;
      console.log("delete_user: "+ id);

      var this_tmp = this;

      $.ajax( {        
        url: "http://localhost:8000/api/users/"+id, 
        type: "DELETE",
        contentType: "application/json",
        dataType: "json",
        success: function(rs){
          // this_tmp.setState({data:rs});
          console.log(rs);
          if (rs.success){
            this_tmp.updateState();
          }
          else{
            alert("Delete error");
          }
        },
        error(e, rs){
          alert("Delete error");
        }
      });
   }
   render() {
      var this_t = this;
      return (

         <div>
            <button onClick={this.updateState}>Refresh</button>
            <h3>List user:</h3>
            <div>
            <table>
            <tr>
            <th>Username</th><th>First name</th><th>Last name</th><th>Operations</th>
            </tr>
            {
              this.state.data.map(function(u){
                return (
                <tr>
                  <td>{u.username}</td>
                  <td>{u.first_name}</td>
                  <td>{u.last_name}</td>
                  <td>
                    <span><button onClick={(e)=>this_t.delete_user(e)} data-id={u._id}>delete</button></span>
                    <span><a href={"users/edit/"+u._id}>edit</a></span>
                  </td>
                  
                </tr>
                );
              })
            } 
            </table>
          </div>
         </div>
      );
   }
}
export default App;