import React from 'react';
import $ from 'jquery'


class App extends React.Component {
   constructor(props) {
      super(props);
      
      this.state = {
         data: 'Initial data...'
      }
      this.login = this.login.bind(this);
   };
   login(e) {
      console.log("Login")
      var this_tmp = this;
      var data = {
        "username": this.refs.username.value,
        "password": this.refs.password.value
      }
      console.log(JSON.stringify(data));
      // return true;
      $.ajax( {        
        url: "http://localhost:8000/api/users/login", 
        type: "POST",
        data:JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function(rs){
          if (rs.success){
            // alert("thanh cong")
            window.location.replace("users");
          }
          else{
            alert("That bai: " + rs.message)
          }
          console.log(rs);
        },
        error: function(e){
          alert("Loi")
        }
      });
   }
   render() {
      return (
         <div>
            <div>
              <label>User name:</label>
              <input type="text" ref="username"/>
            </div>

            <div>
              <label>Password:</label>
              <input type="password" ref="password"/>
            </div>

            <div>
              
              <button onClick={this.login}>Login</button>
            </div>
            <a href="/register">Register</a>

         </div>
      );
   }
}
export default App;