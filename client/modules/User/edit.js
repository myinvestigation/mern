import React from 'react';
import $ from 'jquery'

class App extends React.Component {
   constructor(props) {
      super(props);
      
      this.state = {
        data: 'Initial data...',
        users:[],
        user:{},
        common:{
          is_add: !props.params.id
        },

         
      }
      
      this.submit_form = this.submit_form.bind(this);
      this.changeUser = this.changeUser.bind(this);
      this.change_inputs = this.change_inputs.bind(this);
      this.change_confirm_password = this.change_confirm_password.bind(this);
      
   };
   componentDidMount() {
      if (!this.state.common.is_add)
        this.loadForm();
   }

   loadForm() {
      var this_tmp = this;
      $.ajax( {        
        url: "http://localhost:8000/api/users/"+ this_tmp.props.params.id, 
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        success: function(rs){
          // var u = rs[0];
          // this_tmp.setState({users:rs});
          // if (this_tmp.props.parames.id) {
          //   var current_id = this_tmp.props.parames.id;
          //   rs.forEach(function(item){
          //     if(item._id == current_id){
          //       // u = item;
          //       this.setState({user:item});
          //     }
          //   })
          // }
          // else 
          //   this_tmp.setState({user:rs[0]});
          this_tmp.setState({user:rs})

          
        }
      });
   };
   submit_form(e) {
     if (this.state.common.is_add) {
        this.add_submit(e);
     }
     else{
        this.edit_submit(e)
     }
   };
   add_submit(e) {
      console.log("add_submit");
      if (!this.change_confirm_password(e))
        return;
      var this_tmp = this;
      var data = {
        "username": this.state.user.username,
        "first_name": this.state.user.first_name,
        "last_name": this.state.user.last_name,
        "dob": this.state.user.dob,
        "email": this.state.user.email,
        "mobile_number": this.state.user.mobile_number,
        "password": this.state.user.password,
        "remark": this.state.user.remark
      }
      console.log(JSON.stringify(data));
      // return true;
      $.ajax( {        
        url: "http://localhost:8000/api/users", 
        type: "POST",
        data:JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function(rs){
          alert("thanh cong")
          console.log(rs);
          // this_tmp.setState({common:{message:"Success"}});
          window.location.replace("login");
        },
        error: function(e){
          alert("that bai")
          // this_tmp.state.common.message = "Error: Submit faild";
          // this_tmp.setState(this_tmp.state);
        }
      });

   }
   edit_submit(e) {
      console.log("edit_submit");
      var this_tmp = this;
      var data = {
        // "_id": this.state.user._id,
        "first_name": this.state.user.first_name,
        "last_name": this.state.user.last_name,
        "dob": this.state.user.dob,
        "email": this.state.user.email,
        "mobile_number": this.state.user.mobile_number,
        "remark": this.state.user.remark
      }
      console.log(JSON.stringify(data));
      // return true;
      $.ajax( {        
        url: "http://localhost:8000/api/users/"+this.state.user._id, 
        type: "PUT",
        data:JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function(rs){
          alert("thanh cong")
          console.log(rs);
          this_tmp.setState({common:{message:"Success"}});
        },
        error: function(e){
          alert("that bai")
          this_tmp.setState({common:{message:"Error: Submit faild"}});
        }
      });

   };
   changeUser(e) {
    console.log("changeUser:"+e.target.value);
      var this_t = this;
     this.state.users.forEach(function(item) {
        if (item._id == e.target.value) {
          this_t.setState({user: item});
          return;
        }
     });
   };

   change_inputs(e) {
    console.log("change_inputs: " + e.target.id)
    this.state.user[e.target.id] = e.target.value;
    this.setState(this.state.user)
    console.log(this.state.user)
    if (e.target.id=="password"){
      // this.setMessage("");
      this.change_confirm_password(e);
    }
   }

   change_confirm_password(e) {
    console.log("change_confirm_password");
    if (this.inputs.password.value != this.inputs.confirm_password.value){
      this.setMessage("Error: Confirm password is not match");
      return false;
    }
    else{
      this.setMessage("");
      return true;
    }
   }
   setMessage(str) {
    this.state.common.message=str;
    this.setState(this.state.common);
   }

   render() {
      return (
         <div>
            
            
            <Content user = {this.state.user} ref={(node)=>{this.inputs = node;}}
               submit_form = {this.submit_form} common={this.state.common}
               change_inputs={this.change_inputs}
               confirm_password={this.change_confirm_password}></Content>
         </div>
      );
   }
}

class Select_user extends React.Component {
   render() {
      return (
        <div>
            <label>Select user: </label>
            <select onChange={this.props.change}>
              {
                this.props.users.map(function(u){
                  return (<option value={u._id}>{u.username}</option>)
                })
              }
            </select>
         </div>

      );
   }
}
class Content extends React.Component {
  constructor(props) {
      super(props);
      
      this.state = {
         user:props.user,
         common:props.common
      }

   };

   render() {
      
      return (
         <div>
            {
              this.props.common.is_add?(
                <div>
                  <label>User name:</label>
                  <input ref={(node) => { this.username = node; }} 
                    type = "text" value = {this.props.user.username}
                    id="username"
                    onChange={this.props.change_inputs}/>
                </div>
              ):("")
            }
            <div>
              <label>First name:</label>
              <input ref={(node) => { this.first_name = node; }} 
                type = "text" value = {this.props.user.first_name}
                id="first_name"
                onChange={this.props.change_inputs}/>
            </div>

            <div>
              <label>Last name:</label>
              <input ref={(node) => { this.last_name = node; }} 
                type = "text" value = {this.props.user.last_name}
                id="last_name"
                onChange={this.props.change_inputs}/>
            </div>

            <div>
              <label>Date of birth:</label>
              <input ref={(node) => { this.dob = node; }} 
                type = "text" value = {this.props.user.dob}
                id="dob"
                onChange={this.props.change_inputs}/>
            </div>

            <div>
              <label>Email:</label>
              <input ref={(node) => { this.email = node; }} 
                type = "text" value = {this.props.user.email}
                id="email"
                onChange={this.props.change_inputs}/>
            </div>

            <div>
              <label>Phone number:</label>
              <input ref={(node) => { this.mobile_number = node; }} 
                type = "text" value = {this.props.user.mobile_number}
                id="mobile_number"
                onChange={this.props.change_inputs}/>
            </div>
            <div>
              <label>Remark:</label>
              <input ref={(node) => { this.remark = node; }} 
                type = "text" value = {this.props.user.remark}
                id="remark"
                onChange={this.props.change_inputs}/>
            </div>

            { 
              this.props.common.is_add? (
                <div>
                  <div>
                    <label>Password:</label>
                    <input ref={(node) => { this.password = node; }} 
                      type = "text" value = {this.props.user.password}
                      id="password"
                      onChange={this.props.change_inputs}/>
                  </div>
                  <div>
                    <label>Confirm:</label>
                    <input ref={(node) => { this.confirm_password = node; }} 
                      type = "text"
                      id="confirm_password"
                      onChange={this.props.confirm_password}/>
                  </div>
                </div>
              ):("")
            }

            <div>{this.props.common.message}</div>
            <div>
              <button onClick={this.props.submit_form}>Save</button>
            </div>
         </div>
      );
   }
}
export default App;